from mpmath import *

mp.prec = 128
display_digits = 10


s = mpf('0.5')
a = mpf('3.8')
file_path = ''
file_path = 'PythonOutputLog_PREC' + str(mp.prec) + '_ALPHA' + nstr(a, display_digits) + '.txt'
file_path = file_path.replace('.', '', 1)
file = open(file_path, "w")
file.write('s = ' + nstr(s, display_digits) + ' prec = ' + str(mp.prec) + ' alpha =' + nstr(a, display_digits))
file.write('\n\r')

for i in range(1, 101):
    s = (s * a) * (mpf('1') - s)
    if i % 10 == 0:
        file.write(''.join([str(temp_value) for temp_value in [i, ': ', nstr(s, display_digits)]]))
        file.write('\n\r')
file.close()
