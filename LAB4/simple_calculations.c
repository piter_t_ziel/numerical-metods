#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <mpfr.h>
#include <string.h>
#include <math.h>

#define DEFAULT_PRECISION 256
#define START_VALUE 0.5L
#define DEFAULT_ALPHA 3.0L

#define PRINT_ONE_PER_N 10
#define CALCULATION_LOOPS_NR 100
#define OUTPUT_FILE_NAME "Output_log"

int main()
{    char file_name[40] = {};
    char tmp[20]= {};
    strcat(file_name, OUTPUT_FILE_NAME);
    sprintf(tmp, "_PREC%d", DEFAULT_PRECISION);
    strcat(file_name, tmp);
    sprintf(tmp, "_ALPHA%d", (int)roundf((DEFAULT_ALPHA*10)));
    strcat(file_name, tmp);   
    strcat(file_name, ".txt");

    FILE *fp;

    /* Open new file */
    if ((fp = fopen(file_name, "w")) == NULL)
    {
        perror(file_name);
        return 1;
    }


    fprintf(fp, "Bit precision: %d, alpha: %LF, start value: %LF\n\r\n\r", DEFAULT_PRECISION, DEFAULT_ALPHA, START_VALUE);
    fprintf(fp, "Iteration |   Single precision       "
    "|   Dpuble Precision       |    Extended prec           |      %d prec \n\n", DEFAULT_PRECISION);

    /* Init variables for calcuations */
    float single_p;
    double double_p;
    long double extended_p;
    mpfr_t multiple_p, alpha, temp1, temp2;

    /* Set the default precision to be at least x bits.*/
    mpfr_set_default_prec((unsigned long int)(DEFAULT_PRECISION));

    /* Init multipleprecision float, set to 0 */
    mpfr_init(multiple_p);
    mpfr_init(alpha);
    mpfr_init(temp1);
    mpfr_init(temp2);

    single_p = (float)START_VALUE;
    double_p = (double)START_VALUE;
    extended_p = (long double)START_VALUE;
    mpfr_set_d(multiple_p, (double)START_VALUE, MPFR_RNDN);
    mpfr_set_d(alpha, (double)DEFAULT_ALPHA, MPFR_RNDN);
    mpfr_set_ui(temp1, 0, MPFR_RNDN);
    mpfr_set_ui(temp2, 0, MPFR_RNDN);

    int i;

    for (i = 1; i <= 100; i++)
    {
        single_p = (float)DEFAULT_ALPHA * single_p * (1.0f - single_p);
        double_p = (double)DEFAULT_ALPHA * double_p * (1.0 - double_p);
        extended_p = (long double)DEFAULT_ALPHA * extended_p * (1.0L - extended_p);
        //temp1 = (1.0L - multiple_p)
        mpfr_ui_sub(temp1, 1, multiple_p, MPFR_RNDN);
        //mpfr_printf("temp1 = (1.0L - multiple_p) = %16.5RNe \n\r", temp1);

        //temp2 = (multiple_p * temp1)
        mpfr_mul(temp2, multiple_p, temp1, MPFR_RNDN);
        //mpfr_printf("temp2 = (multiple_p * temp1) = %16.5RNe \n\r", temp2);

        //multiple_p = alpha * temp2
        mpfr_mul(multiple_p, alpha, temp2, MPFR_RNDN);
        //mpfr_printf("multiple_p = alpha * temp2 = %16.5RNe \n\r", multiple_p);

        if (i % 10 == 0)
        {
            fprintf(fp, "%10i| single_p:%16.8f| double_p:%16.8lf| extended_p:%16.8Lf| ", i, single_p, double_p, extended_p);
            mpfr_fprintf(fp, "mpfr: %16.7RNe \n\r", multiple_p);
        }
    }

    mpfr_clear(multiple_p);
    mpfr_clear(alpha);
    mpfr_clear(temp1);
    mpfr_clear(temp2);
    fclose(fp);

    return 1;
}
