import matplotlib.pyplot as plt
import matplotlib.dates as md
import numpy as np
import datetime as dt

# ---------------------------------------------------
# Setup
# ---------------------------------------------------
delimiter = "  "
AVERAGED_OVER_TIME_S = 86400  # Averaged over 24h
FILE_PATH = "AVERAGE86400"
FILE_LINES_NR = 1460    # Number of lines in file


# ---------------------------------------------------
# Global variables:
# ---------------------------------------------------
lines = []
time_stamps = []
temperature = []
pressure = []
skipped = 0

# ---------------------------------------------------
#   Main program cycle
# ---------------------------------------------------

# Open file and read time stamp, temperature and pressure
file = open(FILE_PATH, "r")  # Averaged data - every point is average of 24h

for i in range(FILE_LINES_NR):
    lines.append(file.readline())   # get one line

    delimiter_pos = lines[i].find(delimiter)    # find delimiter
    time_stamp_tmp = int(lines[i][:delimiter_pos])  # read time stamp

    lines[i] = lines[i][delimiter_pos+len(delimiter):]  # move "pointer" to read temperature
    delimiter_pos = lines[i].find(delimiter)    # find next delimiter
    temperature_tmp = float(lines[i][:delimiter_pos])  # read temperature

    pressure_tmp = float(lines[i][delimiter_pos + len(delimiter):])  # read pressure

    if (not np.isnan(time_stamp_tmp)) and (not np.isnan(temperature_tmp)) and (not np.isnan(pressure_tmp)):
        time_stamps.append(time_stamp_tmp)
        temperature.append(temperature_tmp)
        pressure.append(pressure_tmp)

    else:
        skipped += 1

file.close()

print("Number of read lines :", len(temperature))
print("Number of skipped lines (due to NaN) :", skipped)

# Convert time stumps to dates
dates = []
for i in range(len(time_stamps)):
    dates.append(dt.datetime.fromtimestamp(time_stamps[i]))

# Print dates on x axis
plt.xticks(rotation=25)
ax = plt.gca()
xfmt = md.DateFormatter('%Y-%m-%d')
ax.xaxis.set_major_formatter(xfmt)

# Print temperature plot
plt.plot(dates, temperature, '.')
plt.xlabel('Date')
plt.ylabel('Temperature [°C]')
plt.title("Temperature over time, 24h averaging")
plt.show()

# Print dates on x axis
plt.xticks(rotation=25)
ax = plt.gca()
xfmt = md.DateFormatter('%Y-%m-%d')
ax.xaxis.set_major_formatter(xfmt)

# Print pressure plot
plt.plot(dates, pressure, '.')
plt.xlabel('Date')
plt.ylabel('Pressure [hPa]')
plt.title("Pressure over time, 24h averaging")
plt.show()

# ---------------------------------------------------
#   FFT for temperature
# ---------------------------------------------------
time_to_plot = len(temperature) * 86400  # Time (length of input data)
sample_rate = 1 / 86400   # Number of samples per second
num_samples = int(sample_rate * time_to_plot)   # Total number of samples per second

t = np.linspace(0, time_to_plot, num_samples)

# Calculate FFT and adjust axes
fft_output = np.fft.rfft(temperature)
magnitude_only = [np.sqrt(i.real**2 + i.imag**2) / len(fft_output) for i in fft_output]
frequencies = [(i*1.0/num_samples)*sample_rate for i in range(num_samples//2+1)]

# Remove frequency = 0
magnitude_only = magnitude_only[1:]
frequencies = frequencies[1:]

# Convert frequency to period measured in days
for i in range(len(frequencies)):
    frequencies[i] = 1/(frequencies[i] * 3600 * 24)

plt.plot(frequencies, magnitude_only, 'r', label='Temperature')
plt.xlabel('Period [day]')
plt.ylabel('Magnitude')
plt.title("FFT, data averaged over 24h")
plt.legend()
plt.show()

plt.show()

# ---------------------------------------------------
#   FFT for pressure
# ---------------------------------------------------
time_to_plot = len(pressure) * 86400  # Time (length of input data)
sample_rate = 1 / 86400   # Number of samples per second
num_samples = int(sample_rate * time_to_plot)  # Total number of samples per second

t = np.linspace(0, time_to_plot, num_samples)

# Calculate FFT and adjust axes
fft_output = np.fft.rfft(pressure)
magnitude_only = [np.sqrt(i.real**2 + i.imag**2) / len(fft_output) for i in fft_output]
frequencies = [(i*1.0/num_samples)*sample_rate for i in range(num_samples//2+1)]

# Remove frequency = 0
magnitude_only = magnitude_only[1:]
frequencies = frequencies[1:]

# Convert frequency to period measured in days
for i in range(len(frequencies)):
    frequencies[i] = 1/(frequencies[i] * 3600 * 24)

# Plot
plt.plot(frequencies, magnitude_only, 'r', label='Pressure')
plt.xlabel('Period [day]')
plt.ylabel('Magnitude')
plt.title("FFT, data averaged over 24h")
plt.legend()
plt.show()



