import csv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as md
import datetime as dt

# ---------------------------------------------------
# Setup
# ---------------------------------------------------
FILE_PATH = 'temperatura_28102015_0010.csv'
DATA_CONSOLIDATION_ID_STRING = '--- AVERAGE 1800'
DATA_CONSOLIDATION_TIME = 1800

# ---------------------------------------------------
# Global variables:
# ---------------------------------------------------
start_capturing_data = False
dates = []
time_stamps = []
temperatures = []


# ---------------------------------------------------
# Function defines:
# ---------------------------------------------------
def capture_values(csv_line):
    # Capture values from one csv line
    global dates, time_stamps, temperatures

    # convert strings into numbers
    time_stamp_tmp = int(csv_line[1])
    temperature_tmp = float(csv_line[2])

    # Check if there are no NaNs
    if np.isnan(time_stamp_tmp) or np.isnan(temperature_tmp):
        print('Skipping NaN')
        return

    # Append values to global lists
    dates.append(csv_line[0])
    time_stamps.append(time_stamp_tmp)
    temperatures.append(temperature_tmp)

    print("Date:{0}, time_stump: {1}, temp: {2}°C ".format(csv_line[0], time_stamp_tmp, temperature_tmp))


# ---------------------------------------------------
#   Main program cycle
# ---------------------------------------------------

# Import CSV file
with open(FILE_PATH, 'r') as csv_file:
    csv_reader = csv.reader(csv_file)

    for line in csv_reader:
        if (len(line) != 3) and (DATA_CONSOLIDATION_ID_STRING in line[0]):
            print("Start capturing data: ", DATA_CONSOLIDATION_ID_STRING)
            start_capturing_data = True
            continue

        # If data if a part of chosen consolidation time (DATA_CONSOLIDATION_ID_STRING)
        # and is valid (has 3 fields) copy it.
        if start_capturing_data and (len(line) is 3):
            capture_values(line)

        if start_capturing_data and (len(line) is not 3):
            print("Stop capturing data: ", DATA_CONSOLIDATION_ID_STRING)
            start_capturing_data = False
            break

# Convert time stumps to dates
for i in range(len(time_stamps)):
    dates[i] = dt.datetime.fromtimestamp(time_stamps[i])


# Print dates on x axis
plt.xticks(rotation=25)
axis_x = plt.gca()
xfmt = md.DateFormatter('%Y-%m-%d')
axis_x.xaxis.set_major_formatter(xfmt)


# Print temperatures plot
plt.plot(dates, temperatures)
plt.xlabel('Date')
plt.ylabel('Temperature [°C]')
plt.title("Temperature over time, "+DATA_CONSOLIDATION_ID_STRING+'\n File: '+FILE_PATH)
plt.show()

# Calculate gradient
x = time_stamps
dx = x[1]-x[0]
y = temperatures
temperature_gradient = np.gradient(y, dx)

# Multiply values to get °C/day
temperature_gradient *= 24 * 3600 / DATA_CONSOLIDATION_TIME

# Create figure and axis objects with subplots()
figure, axis_x = plt.subplots()
plt.xticks(rotation=25)
xfmt = md.DateFormatter('%Y-%m-%d')
axis_x.xaxis.set_major_formatter(xfmt)

# Make a plot
axis_x.plot(dates, temperatures, color="blue")
axis_x.set_xlabel('Date')
axis_x.set_ylabel('Temperature [°C]', color="blue", )

# Twin object for two different y-axis on the sample plot
axis_x_2 = axis_x.twinx()

# Make a plot with different y-axis using second axis object
axis_x_2.plot(dates, temperature_gradient, color="red")
axis_x_2.set_ylabel('Temperature gradient [°C/day]', color="red")
plt.title("Temperature over time, "+DATA_CONSOLIDATION_ID_STRING+'\n File: '+FILE_PATH)
plt.show()



