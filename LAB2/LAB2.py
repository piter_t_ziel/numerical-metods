import csv
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.interpolate import lagrange
from scipy.interpolate import BarycentricInterpolator

# ---------------------------------------------------
# Setup
# ---------------------------------------------------
FILE_PATH = 'temperatura_28102015_0010.csv'
DATA_CONSOLIDATION_ID_STRING = '--- MIN 300'
DATA_CONSOLIDATION_TIME = 300
ONE_NODE_PER_N = 24
# Set parameters for interpolation
p = 3  # stopień wielomianu interpolacyjnego


# ---------------------------------------------------
# Global variables:
# ---------------------------------------------------
start_capturing_data = False

time_stamps = []
temperatures = []

nodes_time_stamps = []
nodes_temperatures = []


# ---------------------------------------------------
# Function defines:
# ---------------------------------------------------
def capture_values(csv_line):
    # Capture values from one csv line
    global time_stamps, temperatures

    # convert strings into numbers
    time_stamp_tmp = int(csv_line[1])
    temperature_tmp = float(csv_line[2])

    # Check if there are no NaNs
    if np.isnan(time_stamp_tmp) or np.isnan(temperature_tmp):
        print('Skipping NaN')
        return

    # Append values to global lists
    time_stamps.append(time_stamp_tmp)
    temperatures.append(temperature_tmp)

    print("Date:{0}, time_stump: {1}, temp: {2}°C ".format(csv_line[0], time_stamp_tmp, temperature_tmp))


# ---------------------------------------------------
#   Main program cycle
# ---------------------------------------------------

# Import CSV file
with open(FILE_PATH, 'r') as csv_file:
    csv_reader = csv.reader(csv_file)

    for line in csv_reader:
        if (len(line) != 3) and (DATA_CONSOLIDATION_ID_STRING in line[0]):
            print("Start capturing data: ", DATA_CONSOLIDATION_ID_STRING)
            start_capturing_data = True
            continue

        # If data if a part of chosen consolidation time (DATA_CONSOLIDATION_ID_STRING)
        # and is valid (has 3 fields) copy it.
        if start_capturing_data and (len(line) is 3):
            capture_values(line)

        if start_capturing_data and (len(line) is not 3):
            print("Stop capturing data: ", DATA_CONSOLIDATION_ID_STRING)
            start_capturing_data = False
            break

# Convert timestamp to h
time_stamps = np.subtract(time_stamps, time_stamps[0])
time_stamps = np.divide(time_stamps, 3600)

# Make new, reduced list containing  nodes
nodes_nr = int(len(time_stamps) / ONE_NODE_PER_N)
for i in range(nodes_nr):
    nodes_time_stamps.append(time_stamps[i * ONE_NODE_PER_N])
    nodes_temperatures.append(temperatures[i * ONE_NODE_PER_N])

fig1 = plt.figure(1)
fig2 = plt.figure(2)
ax1 = fig1.add_subplot(111)
ax2 = fig2.add_subplot(111)


options = ('linear', 'nearest', 'zero', 'slinear', 'quadratic', 'cubic', p)

for o in options:
    f = interp1d(nodes_time_stamps, nodes_temperatures, kind=o)
    interpolated_values = f(time_stamps[:-ONE_NODE_PER_N])

    plt.figure(1)
    plt.plot(time_stamps[:-ONE_NODE_PER_N], interpolated_values, label=o)

    interpolation_errors = np.subtract(temperatures[:-ONE_NODE_PER_N], interpolated_values)
    plt.figure(2)
    plt.plot(time_stamps[:-ONE_NODE_PER_N], interpolation_errors, label=o)


# lagrange interpolation
f_lagrange = lagrange(nodes_time_stamps, nodes_temperatures)
interpolated_values = f_lagrange(time_stamps[:-ONE_NODE_PER_N])

plt.figure(1)
plt.plot(time_stamps[:-ONE_NODE_PER_N], interpolated_values, label='lagrange')

interpolation_errors = np.subtract(temperatures[:-ONE_NODE_PER_N], interpolated_values)
plt.figure(2)
plt.plot(time_stamps[:-ONE_NODE_PER_N], interpolation_errors, label='lagrange')


# Barycentric Interpolation
f_BarycentricInterpolator = BarycentricInterpolator(nodes_time_stamps, nodes_temperatures)
interpolated_values = f_BarycentricInterpolator(time_stamps[:-ONE_NODE_PER_N])

plt.figure(1)
plt.plot(time_stamps[:-ONE_NODE_PER_N], interpolated_values, label='BarycentricInterpolator')

interpolation_errors = np.subtract(temperatures[:-ONE_NODE_PER_N], interpolated_values)
plt.figure(2)
plt.plot(time_stamps[:-ONE_NODE_PER_N], interpolation_errors, label='BarycentricInterpolator')

# Plot interpolation effects
plt.figure(1)
plt.plot(nodes_time_stamps, nodes_temperatures, 'o', label='nodes')
plt.plot(time_stamps, temperatures, '-.', label='original')
plt.legend()
plt.xlabel('Time [h]')
plt.ylabel('Temperature [°C]')
plt.title("Interpolation effects, "+DATA_CONSOLIDATION_ID_STRING+'\n File: '+FILE_PATH +
          '\n One node per: '+str(ONE_NODE_PER_N)+' original points')


# Plot interpolation errors
plt.figure(2)
plt.legend()
plt.xlabel('Time [h]')
plt.ylabel('Temperature [°C]')
plt.title("Interpolation errors, "+DATA_CONSOLIDATION_ID_STRING+'\n File: '+FILE_PATH +
          '\n One node per: '+str(ONE_NODE_PER_N)+' original points')


plt.show()
