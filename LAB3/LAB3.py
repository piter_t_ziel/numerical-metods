import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate as si


FILE_PATH = "AVERAGE300"
polyfit_deg = 3
display_nr_points = 2000
data = np.loadtxt(FILE_PATH)

# Convert timestamps to hours
data[:, 0] = np.subtract(data[:, 0], data[0, 0])
data[:, 0] = np.divide(data[:, 0], 3600)

# Plot loaded data
fig3 = plt.figure(3)
ax2 = fig3.add_subplot(111)
plt.plot(data[:, 0], data[:, 1], '.')
plt.xlabel('Time [h]')
plt.ylabel('Pressure [°C]')
plt.title('Data loaded from '+FILE_PATH)

# Plot linear approximation
A = np.vstack([data[:, 0], np.ones(len(data[:, 0]))]).T
m, c = np.linalg.lstsq(A, data[:, 1], rcond=None)[0]  # Don't care about residuals right now
fig4 = plt.figure(4)
ax = fig4.add_subplot(111)
plt.plot(data[:, 0], data[:, 1], 'bo', label="Data")
plt.plot(data[:, 0], m*data[:, 0]+c, 'r--', label="y="+str(m)+'*x'+'+'+str(c))
plt.legend()
plt.xlabel('Time [h]')
plt.ylabel('Pressure [°C]')
plt.title('Linear approximation')


# Create figures for plotting approximations and approximation errors
fig1 = plt.figure(1)
fig2 = plt.figure(2)
ax1 = fig1.add_subplot(111)
ax2 = fig2.add_subplot(111)

fig1 = plt.figure(1)
plt.xlabel('Time [h]')
plt.ylabel('Pressure [°C]')
plt.title('Pressure over time')

fig2 = plt.figure(2)
plt.xlabel('Time [h]')
plt.ylabel('Pressure error [°C]')
plt.title('Approximation error')

fig2 = plt.figure(1)
plt.plot(data[:, 0], data[:, 1], '.', label='File: '+FILE_PATH)

# poly_deg_options = (2, 3, 4, 5, 6, 7)
# poly_deg_colors = ('r', 'g', 'k', 'y', 'c', 'b')

poly_deg_options = (8, 9, 10, 11, 12, 13)
poly_deg_colors = ('r', 'g', 'k', 'y', 'c', 'b')

# Create timestamps for display purpose
new_x = np.linspace(data[0, 0], data[-1, 0], display_nr_points)

# Check polyfit approximations
for deg, color in zip(poly_deg_options, poly_deg_colors):
    pf_f = np.polyfit(data[:, 0], data[:, 1], deg)
    polyfit_polynomial = np.poly1d(pf_f)
    plt.figure(1)
    plt.plot(new_x, polyfit_polynomial(new_x), color, label=('polyfit, deg: '+str(deg)))
    error = np.subtract(data[:, 1], polyfit_polynomial(data[:, 0]))
    plt.figure(2)
    plt.plot(data[:, 0], error, color, label=('polyfit, deg: ' + str(deg)))


plt.figure(1)
plt.legend()
plt.grid()
plt.figure(2)
plt.legend()
plt.grid()

plt.show()
